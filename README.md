# TW

## Stocker et sauvegarder mes tiddlywiki de travail
- HALsaisie
- HN-UPPA
- ZoteroPlus


## Partager mon fichier empty.html

- version 5.1.22

### Customisations : 

#### Module `details`
`<$details summary="titre"></$details>`

#### CSS
- notes
- tableaux

#### Plugins
- Wrappers
    - Markdown
    - Mermaid [source](https://gt6796c.github.io/)
    - Railroad
    - KaTeX
    - XMLdom
- FontAwesome 
    - [site](http://fa5-free-svg.tiddlyspot.com/)
    - [Trouver une icone](https://thediveo.github.io/TW5FontAwesome/output/fontawesome.html#Cheatsheet)
- Dynamic tables [site](https://ooktech.com/jed/ExampleWikis/DynamicTables/)
- D3 [démo](https://tiddlywiki.com/plugins/tiddlywiki/d3/)
- QRcodes générator
- xlsx-utils [démo](http://tiddlywiki.com/prerelease/editions/xlsx-utils/)
- Bibtex importer
- Twitter
- Slicing tiddlers


